/**
 * View Models used by Spring MVC REST controllers.
 */
package org.ekans.cahier.web.rest.vm;
