/**
 * Data Access Objects used by WebSocket services.
 */
package org.ekans.cahier.web.websocket.dto;
